#!/usr/bin/env python3

#script that generates a .csv file containing
#the media info of all files in a directory

from pymediainfo import MediaInfo
import csv
import sys
from os import listdir
import os.path
import argparse
import collections

csv_columns = list()
#General
csv_columns.append("Complete name")
csv_columns.append("Format")
csv_columns.append("File size")
csv_columns.append("File size - Pretty")
#Video
csv_columns.append("Width")
csv_columns.append("Height")
csv_columns.append("Codec")
csv_columns.append("Frame rate")
csv_columns.append("Bit rate")
csv_columns.append("Bit depth")
csv_columns.append("Duration")
csv_columns.append("Duration - Pretty")
csv_columns.append("ReFrames")
csv_columns.append("Scan Type")

def dirLoop(dirpath, csvWriter, recursive):
    for filename in listdir(dirpath):
        print("Filename: ", dirpath + filename)
        print("isDir: ", os.path.isdir( dirpath + filename ) )
        if os.path.isdir( dirpath + filename ):
            dirLoop( dirpath + filename + "/", csvWriter, recursive )
        else:
            csvWriter.writerow(getMediaCSV(dirpath + filename))
    return csvWriter

def print_dict( d ):
    d = dict( collections.OrderedDict( sorted( d.items() ) ) )
    for key, value in d.items():
        print( "{0}: {1}".format( key, value ) )

def printMediaCSV( filepath ):
    media_info = MediaInfo.parse( filepath )
    for track in media_info.tracks:
        if track.track_type == 'General' or track.track_type == 'Video':
            print( "type: {0}: ".format( track.track_type ) )
            print_dict( track.__dict__ )
            print()

def getMediaCSV(filepath):
    csvrow = dict()
    media_info = MediaInfo.parse(filepath)
    for track in media_info.tracks:
        if track.track_type == 'General':
            filename = getattr(track, "complete_name").split("/")
            csvrow["Complete name"] = filename[len(filename) - 1]
            csvrow["Format"] = getattr(track, "commercial_name")
            csvrow["File size"] = getattr(track, "file_size")
            csvrow["File size - Pretty"] = getattr(track, "other_file_size")[0]
        if track.track_type == 'Video':
            csvrow["Width"] = getattr(track, "width")
            csvrow["Height"] = getattr(track, "height")
            csvrow["Codec"] = getattr(track, "codec_id")
            csvrow["Frame rate"] = getattr(track, "frame_rate")
            csvrow["Bit rate"] = getattr(track, "other_bit_rate")
            csvrow["Bit depth"] = getattr(track, "bit_depth")
            csvrow["Duration"] = getattr(track, "duration")
            csvrow["Duration - Pretty"] = getattr(track, "other_duration")
            csvrow["ReFrames"] = getattr(track, "codec_settings_refframes")
            csvrow["Scan Type"] = getattr(track, "scan_type")
    print(csvrow)
    return csvrow

def openCsv(csvfile):
    header = True
    if os.path.exists(csvfile) == False:
        print("does not exists")
        header = False
    csvwriter = csv.DictWriter(open(csvfile, 'a'), csv_columns)
    if header == False:
        csvwriter.writeheader()
    return csvwriter


#main
description = "Extracts mediainfo data from a file or directory"
parser = argparse.ArgumentParser( description=description )
parser.add_argument( '-i', '--input', dest='input_path',
        help="path to file or directory" )
parser.add_argument( '-o', '--output', dest='output',
        help="file to save output, mendatory and only for a directory" )
parser.add_argument( '-r', '--recursive', const=True, nargs='?',
        dest='recursive', default=False,
        help="recursivelly use mediainfo" )
args = parser.parse_args()
input_path = args.input_path
output = args.output
recursive = args.recursive

if input_path != None:
    if os.path.exists( input_path ) == False:
        print( "Error: input: No such file or directory" )
        exit( 1 )
    if os.path.isdir( input_path ) == True:
        if output == None:
            print( "Error: You didn't specify an output file" )
            exit( 1 )
        print( "Input directory: " + str( input_path ) )
        csvWriter = openCsv( output )
        #for filename in os.listdir( input_path ):
        #    csvWriter.writerow( getMediaCSV( input_path + filename ) )
        dirLoop( input_path, csvWriter, recursive )
    else:
        printMediaCSV( input_path )
else:
    parser.print_help()

