To compile thumbnailer using another libvlc than from your system,
a libvlc4 compiled version for example
```
export PKG_CONFIG_PATH="$PATH_TO_LIBVLC/lib/pkgconfig"
gcc -pedantic -Wall -Wextra  -pthread -D _GNU_SOURCE vlc-thumbnailer.c -o vlc-thumbnailer `pkg-config --cflags --libs libvlc`
```

Then to run:
```
LD_LIBRARY_PATH=$PATH_TO_LIBVLC/lib/ ./vlc-thumbnailer $TIMESTAMP $SAMPLE $FILENAME 
```
