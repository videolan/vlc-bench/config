#!/bin/bash

# Takes a screenshot from a video using the vlc scene filter
# This is mainly for testing purposes, testing the scene filter
# and it's options

if [ "$#" -eq 1 ] ; then
    LENGTH=`./getSampleLength.sh $1`
    INDEX=1
    TIME[0]=`bc -l <<< "$LENGTH * 0.25"`
    TIME[1]=`bc -l <<< "$LENGTH * 0.50"`
    TIME[2]=`bc -l <<< "$LENGTH * 0.75"`
    for timestamp in "${TIME[@]}" ; do
        vlc \
            --fullscreen \
            --aspect-ratio=19:9 \
            --video-filter=scene \
            --start-time=$timestamp \
            --stop-time=`bc -l <<< "$timestamp + 0.5"` \
            --scene-ratio=1 \
            --scene-format=png \
            --scene-path=./dir_scenes \
            --scene-prefix=snap \
            $1 \
            vlc://quit;
        SCREENSHOT="screenshot_${INDEX}.png"
        mv ./dir_scenes/snap00002.png ./dir_scenes/$SCREENSHOT
        rm ./dir_scenes/snap?????.png
        INDEX=`expr $INDEX + 1`
    done
else
    echo "usage: ./vlc-scene.sh filepath"
fi
