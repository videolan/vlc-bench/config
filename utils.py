#!/usr/bin/env python3

import json
import hashlib
from pymediainfo import MediaInfo

def get_checksum(filepath):
    filestr = open(filepath, "rb")
    hexhash = hashlib.sha512(filestr.read()).hexdigest()
    return hexhash

def load_json( filename ):
    try:
        jsonfile = open( filename, "r+" )
        jsonstr = jsonfile.read()
        data = json.loads( jsonstr )
        jsonfile.close()
    except Exception as e:
        print( "Failed to load file:  {0}".format( e ) )
        return None
    return data

def save( filename, index ):
    jsonfile = open( filename, "w+" )
    datastr = json.dumps( index, indent=4, separators=(",", ": " ) )
    jsonfile.seek( 0 )
    jsonfile.write( datastr )
    jsonfile.truncate()
    jsonfile.close()

def is_int( string ):
    try:
        int( string )
        return True
    except ValueError:
        return False

def get_codec_from_mediainfo( codecinfo ):
    codec_id = codecinfo[ "codec_id" ]
    format_str = codecinfo[ "format" ]
    version = codecinfo[ "format_version" ]

    if version != None:
        codec = format_str.split(" ")[ 0 ]
        if version == "Version 2":
            codec += "_2V"
        else:
            codec += "_" + version
        return codec
    elif ( format_str == "VC-1" or format_str == "AVC" or
            format_str == "VP8" or format_str == "VP9" or
            format_str == "HEVC" ) or codec_id == None or is_int( codec_id ):
        return format_str
    elif codec_id == "XVID":
        return "MPEG-4"
    else:
        return codec_id


def get_mediainfo( filepath ):
    csvinfo = dict()
    codecinfo = dict()
    media_info = MediaInfo.parse( filepath )
    for track in media_info.tracks:
        if track.track_type == "Video":
            csvinfo[ "Width" ] = getattr( track, "width" )
            csvinfo[ "Height" ] = getattr( track, "height" )
            csvinfo[ "Fps" ] = getattr( track, "frame_rate" )
            csvinfo[ "Bit depth" ] = getattr( track, "bit_depth" )
            csvinfo[ "Codec" ] = getattr( track, "codec_id" )

            codecinfo[ "codec_id" ] = getattr( track, "codec_id" )
            codecinfo[ "format" ] = getattr( track, "format" )
            codecinfo[ "format_version" ] = getattr( track, "format_version" )
            csvinfo[ "Codec" ] = get_codec_from_mediainfo( codecinfo )
    return csvinfo

def get_name_from_mediainfo( i, mediainfo, file_extention ):
    name = str( i ) + "_"
    name += str( mediainfo[ "Codec" ] ).replace( "/", "-" ) + "_"
    if mediainfo[ "Bit depth" ] != None:
        name += str( mediainfo[ "Bit depth" ] ) + "bits_"
    name += str( mediainfo[ "Width" ] ) + "x"
    name += str( mediainfo[ "Height" ] ) + "_"
    name += str( mediainfo[ "Fps" ] ).replace( ".", "_") + "fps."
    name += file_extention
    return name


