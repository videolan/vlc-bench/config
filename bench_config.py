#!/usr/bin/env python3

import argparse
import sys
import json
from pprint import pprint
from subprocess import check_output
from subprocess import call
import dbus
import sys
import os
import time
from Xlib import display
import threading
from PIL import Image
from shutil import copyfile, rmtree
import hashlib
from enum import Enum
import pyscreenshot
import utils
import image_comparison as ic
import re

vlcPath = "vlc"
MAJOR_SAMPLELIST_VERSION = 2

def exit(msg):
    print(msg)
    exit(1)

class KeyHandler:

    def __init__(self):
        self.display = display.Display()
        self.last_pressed = None

    def fetch_key(self):
        keypresses_raw = self.display.query_keymap()

        pressed = None
        for i, k in enumerate(keypresses_raw):
            if (i == 1 and k == 2):
                print("esc")
                pressed = "<esc>"
            elif (i == 4 and k == 16):
                print("enter")
                pressed = "<enter>"

        if pressed == self.last_pressed:
            pressed = None
            state_changed = False
        else:
            state_changed = True

        return state_changed, pressed

# function for starting vlc in other thread.


def startVlc():
    os.popen("{0} --fullscreen".format(vlcPath))


class VlcDbus:

    def __init__(self):
        self.thread = threading.Thread(target=startVlc)
        self.thread.start()
        time.sleep(2)
        self.session_bus = dbus.SessionBus()
        self.vlcHandle = self.session_bus.get_object('org.mpris.MediaPlayer2.vlc',
                                                     '/org/mpris/MediaPlayer2')
        # interface handles
        self.vlcPlayer = dbus.Interface(
            self.vlcHandle, "org.mpris.MediaPlayer2.Player")
        self.vlcProp = dbus.Interface(
            self.vlcHandle, "org.freedesktop.DBus.Properties")
        self.vlc = dbus.Interface(self.vlcHandle, "org.mpris.MediaPlayer2")

    def quit(self):
        self.vlc.Quit()

    def play(self):
        self.vlcPlayer.Play()

    def pause(self):
        self.vlcPlayer.Pause()

    def getPosition(self):
        property = self.vlcProp.Get(
            "org.mpris.MediaPlayer2.Player", "Position")
        # result is in microseconds
        #it is needed in milliseconds
        property /= 1000
        return (property)

    def getPlaybackStatus(self):
        property = self.vlcProp.Get(
            "org.mpris.MediaPlayer2.Player", "PlaybackStatus")
        return property

    def open(self, uri):
        self.vlcPlayer.OpenUri(uri)


class JsonHandler:

    def __init__(self, path="./samples"):
        if (not os.path.exists(path)):
            os.mkdir(path)
        elif (not os.path.isdir(path)):
            os.remove(path)
            os.mkdir(path)
        if (not os.path.exists(path + "/config.json")):
            self.jsonfile = open(path + "/config.json", "w+")
        else:
            self.jsonfile = open(path + "/config.json", "r+")

    def getOnlineJsonObject(self):
        jsonstr = self.jsonfile.read()
        jsondata = json.loads(jsonstr)
        return (jsondata)

    def getJsonObject(self):
        jsonstr = self.jsonfile.read()
        #print("jsonstr = " + jsonstr)
        if (jsonstr == ""):
            return dict()
        else:
            jsondata = json.loads(jsonstr)
            return (jsondata)

    # def getTestList(self, mode):
    #     jsonData = self.getJsonObject()
    #     if mode == AddType.SPEED:
    #         if "speed" in jsonData:
    #             testList = jsonData["speed"]
    #         else:
    #             testList = []
    #     else:
    #         if "classic" in jsonData:
    #             testList = jsonData["classic"]
    #         else:
    #             testList = []
    #     return testList

    # def setTestList(self, mode, testList):
    #     jsonData = self.getJsonObject()
    #     if mode == AddType.SPEED:
    #         jsonData["speed"] = testList
    #     else:
    #         jsonData["classic"] = testList
    #     self.setJsonObject(jsonData)

    def printJsonObject(self, jsonObject):
        for unit in jsonObject:
            pprint(unit)
            print()

    def setJsonObject(self, data):
        datastr = json.dumps(data, indent=4, separators=(',', ': '))
        self.jsonfile.seek(0)
        self.jsonfile.write(datastr)
        self.jsonfile.truncate()

    def closeJsonObject(self):
        self.jsonfile.close()

# ScreenshotHandler will handle the storage of all screenshots
# in ./samples/screenshots and then in a directory named as the
# media file which was used to take these screenshots
# ex: ./samples/screenshots/ducks_take_off_h265_10bits_3860x2160_30fps/scr_1.png


class ScreenshotHandler:

    def __init__(self, filename):
        sample_dir = "./samples"
        scr_dir = sample_dir + "/screenshots"
        # scr_dir + filename without the extension
        filedir = scr_dir + "/" + filename.split(".")[0]

        if not os.path.exists(sample_dir):
            print("There is no 'sample' directory\nexiting ...")
            exit(1)
        if not os.path.exists(scr_dir):
            os.mkdir(scr_dir)
        elif not os.path.isdir(scr_dir):
            os.remove(scr_dir)
            os.mkdir(scr_dir)
        self.filedir = filedir
        self.scr_array = list()

    def createDir(self):
        if os.path.exists(self.filedir):
            print("The directory '{0}' already exists"
                  .format(self.filedir))
            rmtree(self.filedir)
        os.mkdir(self.filedir)

    def getCurrentFilename(self):
        return self.filedir + "/screenshot_" + str(len(self.scr_array) + 1) + ".png"

    def takeScreenshot(self):
        im = pyscreenshot.grab()
        if (im != None):
            filename = self.getCurrentFilename()
            self.scr_array.append(filename)
            im.save(filename, "png")
        else:
            print("Failed to get screenshot")
            return False
        return True

    def addScreenshot(self, filename):
        filepath = self.getCurrentFilename()
        os.rename(filename, filepath)
        self.scr_array.append(filepath)

    def getLast(self):
        return self.scr_array[-1]

    def delDir(self):
        rmtree(self.filedir)


def hardSampleLoop(filepath, scr):
    vlc = VlcDbus()
    vlc.open("file://" + filepath)
    time.sleep(2)
    keyHandle = KeyHandler()
    tab = list()
    i = 0
    while vlc.getPlaybackStatus() != "Stopped":
        state_changed, pressed = keyHandle.fetch_key()
        if (state_changed and pressed == "<enter>"):
            vlc.pause()
            position = int(vlc.getPosition())
            if scr.takeScreenshot() == True:
                colorValue = ic.getColorValues(scr.getLast())
                tab.append([position, colorValue])
                i += 1
            else:
                print("Failed to take screenshot")
            vlc.play()
        if (state_changed and pressed == "<esc>" and i >= 2):
            break
    vlc.quit()
    if i == 0:
        return None
    return tab


def selectionLoop(filepath, scr):
    # manual screenshot selection loop
    vlc = VlcDbus()
    vlc.open("file://" + filepath)
    time.sleep(2)
    keyHandle = KeyHandler()
    i = 0
    timestamps = list()
    while vlc.getPlaybackStatus() != "Stopped":
        state_changed, pressed = keyHandle.fetch_key()
        if (state_changed and pressed == "<enter>"):
            vlc.pause()
            position = vlc.getPosition()
            timestamps.append(position)
            time.sleep(1)  # just for visual effect of choosing a timestamp
            i += 1
            vlc.play()
        if (state_changed and pressed == "<esc>" and i >= 2):
            break
    vlc.quit()
    if i == 0:
        return None
    return autoSelectionLoop(filepath, timestamps, scr)

def autoSelectionLoop(filepath, timestamps, scr):
    # Automatic mode takes screenshots using thumbnailer/vlc-thumnailer
    # It takes 3 thumbnails at 25, 50, 75 percent of the sample
    snapshots = list()
    my_env = os.environ.copy()
    for index, time in enumerate(timestamps):
        filename = "screenshot_{0}.png".format(index + 1)
        callParams = list()
        callParams.append("./thumbnailer/vlc-thumbnailer")
        callParams.append(str(time))
        callParams.append(filepath)
        callParams.append(filename)
        print(callParams)
        call(callParams, env=my_env)
        scr.addScreenshot(filename)
        snapshots.append([time, ic.getColorValues(scr.getLast())])
    return snapshots


def autoGetTimestamps(filepath):
    strVidLen = check_output(["./getSampleLength.sh", filepath])
    vidLen = float(strVidLen)
    timestamps = list()
    timestamps.append(int(round(0.25 * vidLen * 1000)))
    timestamps.append(int(round(0.50 * vidLen * 1000)))
    timestamps.append(int(round(0.75 * vidLen * 1000)))
    return timestamps


def checkChecksum():
    dirpath = updateFileName("./samples")
    dirfiles = os.listdir(dirpath)
    jHandle = JsonHandler()
    for filename in dirfiles:
        configEl = getFromConfig(filename)
        if configEl != None and configEl["checksum"] != getChecksum(dirpath + "/" + filename):
            print(filename + ": bad checksum")
        elif configEl == None and filename != "config.json":
            print(filename + ": not in the json config file")


def getChecksum(filepath):
    filestr = open(filepath, "rb")
    hexhash = hashlib.sha512(filestr.read()).hexdigest()
    return hexhash

# gets the name at the end of the given path


def getFileName(filepath):
    tab = filepath.split("/")
    return tab[-1]


class VersionIncrement(Enum):
    MINOR = 0
    PATCH = 1


def setVersion(jsonData, versionIncrement):
    if ("profile" in jsonData.keys() and "version" in jsonData["profile"].keys()):
        version = jsonData["profile"]["version"].split(".")
        if (len(version) == 3):
            if (versionIncrement == VersionIncrement.MINOR):
                version[1] = str(int(version[1]) + 1)
                version[2] = "0"
            elif (versionIncrement == VersionIncrement.PATCH):
                version[2] = str(int(version[2]) + 1)
            else:
                exit("Wrong VersionIncrement value")
            jsonData["profile"]["version"] = ".".join(version)
        else:
            exit("Invalid version format")
    else:
        if "profile" not in jsonData.keys():
            jsonData["profile"] = dict()
        jsonData["profile"]["version"] = str.format("{0}.0.0", MAJOR_SAMPLELIST_VERSION)
    return jsonData


def edit(filename, mode):
    if mode == AddType.SPEED:
        print("Speed tests cannot be edited.")
        exit(1)
    jHandler = JsonHandler()
    # testList = jHandler.getTestList(mode)
    jsonData = jHandler.getJsonObject()
    sample = None
    index = -1
    for i, x in enumerate(testList):
        if x["name"] == filename:
            sample = x
            index = i
            break
    if sample == None:
        print("Sample not found")
        return
    filepath = "samples/" + filename
    scr = ScreenshotHandler(sample["name"])
    scr.createDir()
    if mode == AddType.MANUAL:
        sample["snapshot"] = selectionLoop(filepath, scr)
    elif mode == AddType.AUTO:
        timestamps = autoGetTimestamps(filepath)
        sample["snapshot"] = autoSelectionLoop(filepath, timestamps, scr)
    elif mode == AddType.HARD:
        sample["snapshot"] = hardSampleLoop(filepath, scr)
    else:
        print("Unknown mode.")
    # testList[index] = sample
    # jHandler.setTestList(mode, testList)
    setVersion(jsonData, VersionIncrement.PATCH)
    jHandler.setJsonObject(jsonData)
    jHandler.closeJsonObject()


class ClassicType(Enum):
    SOFTWARE_QUALITY = 1
    SOFTWARE_PLAYBACK = 2
    HARDWARE_QUALITY = 3
    HARDWARE_PLAYBACK = 4


class AddType(Enum):
    # user selects the timestamps then uses auto for screenshots
    MANUAL = 1
    # timestamps are selected at 0.25, 0.50, 0.75 of the file
    AUTO = 2
    # grabs system display, not as convenient, but on some samples
    # the auto mode takes black screenshot
    # because of bad seek, or difficult stream
    HARD = 3
    # Speed test
    SPEED = 4
    # Playback only
    PLAYBACK = 5


def formatClassicTest(sample):
    testConf = dict()
    testConf["quality"] = list()
    testConf["playback"] = list()
    for val in ClassicType:
        test = dict()
        test["original_name"] = sample["original_name"]
        test["name"] = sample["name"]
        test["url"] = sample["url"]
        test["checksum"] = sample["checksum"]
        test["size"] = sample["size"]
        if val == ClassicType.SOFTWARE_QUALITY:
            test["hardware"] = False
            test["snapshot"] = sample["snapshot"]
            testConf["quality"].append(test)
        elif val == ClassicType.SOFTWARE_PLAYBACK:
            test["hardware"] = False
            testConf["playback"].append(test)
        elif val == ClassicType.HARDWARE_QUALITY:
            test["hardware"] = True
            test["snapshot"] = sample["snapshot"]
            testConf["quality"].append(test)
        elif val == ClassicType.HARDWARE_PLAYBACK:
            test["hardware"] = True
            testConf["playback"].append(test)
    print(testConf)
    return testConf


def add(filepath, mode, keepOriginalName):
    # auto is a boolean to add in automatic mode
    # reminders()
    if (not os.path.exists(filepath)):
        print("File not found")
        return
    if mode == AddType.SPEED:
        key = "speed"
    elif mode == AddType.PLAYBACK:
        key = "playback"
    else:
        key = "quality"

    jHandler = JsonHandler()
    jsonData = jHandler.getJsonObject()

    if "quality" not in jsonData:
        jsonData["quality"] = list()
    if "playback" not in jsonData:
        jsonData["playback"] = list()
    if "speed" not in jsonData:
        jsonData["speed"] = list()

    mediainfo = utils.get_mediainfo(filepath)
    original_name = getFileName(filepath)
    if find(original_name, key):
        print("That sample already exists")
        return

    if keepOriginalName:
        name = original_name
    else:
        if key in jsonData and len(jsonData[key]) >= 1:
            index = int(jsonData[key][-1]['name'].split("_")[0]) + 1
        else:
            index = 1
        name = utils.get_name_from_mediainfo(index, mediainfo,
                                         original_name.split(".")[-1])

    scr = ScreenshotHandler(name)
    scr.createDir()
    sample = dict()
    if mode == AddType.MANUAL:
        sample["snapshot"] = selectionLoop(filepath, scr)
    elif mode == AddType.AUTO:
        timestamps = autoGetTimestamps(filepath)
        sample["snapshot"] = autoSelectionLoop(filepath, timestamps, scr)
    elif mode == AddType.HARD:
        sample["snapshot"] = hardSampleLoop(filepath, scr)
    elif mode != AddType.SPEED and mode != AddType.PLAYBACK:
        print("Unknown mode.")
        exit(1)

    # Check that snapshots are not Null
    if mode != AddType.SPEED and mode != AddType.PLAYBACK and sample["snapshot"] == None:
        print("Failed to produce snapshot")
        return
    sample["original_name"] = original_name
    sample["name"] = name
    sample["url"] = name
    sample["checksum"] = getChecksum(filepath)
    sample["size"] = os.path.getsize(filepath)
    if mode == AddType.SPEED:
        sample["hardware"] = False
        jsonData["speed"].append(sample)
    elif mode == AddType.PLAYBACK:
        sample["hardware"] = False
        jsonData["playback"].append(sample)
    else:
        classicTests = formatClassicTest(sample)
        jsonData["quality"] += classicTests["quality"]
        jsonData["playback"] += classicTests["playback"]

    # jHandler.setTestList(mode, testList)
    setVersion(jsonData, VersionIncrement.MINOR)
    jHandler.setJsonObject(jsonData)
    jHandler.closeJsonObject()
    copyfile(filepath, "./samples/" + name)


def delete(filepath):
    jHandle = JsonHandler()
    jsonData = jHandle.getJsonObject()
    name = getFileName(filepath)
    scr = ScreenshotHandler(name)
    obj = None
    for key, value in jsonData.items():
        if key != "profile":
            for element in value:
                if element["name"] == name:
                    obj = element
    if (obj != None):
        jsonData.remove(obj)
        if (not os.path.exists("./samples/" + name)):
            print("File not found")
            return
        os.remove("./samples/" + getFileName(filepath))
        scr.delDir()
        print("Deleted " + filepath)
    else:
        print("Could not find the json object to delete")
    setVersion(jsonData, VersionIncrement.MINOR)
    jHandle.setJsonObject(jsonData)
    jHandle.closeJsonObject()


def mergeDir(path):
    print("Adding content of configuration at: {0}".format(path))
    sampleHandler = JsonHandler("./samples")
    sampleData = sampleHandler.getJsonObject()
    sampleScreenshotDir = "./samples/screenshots/"
    mergeDirHandler = JsonHandler(path)
    mergeDirData = mergeDirHandler.getJsonObject()
    mergeScreenshotDir = path + "/screenshots/"
    index = len(sampleData) + 1
    if not os.path.exists(sampleScreenshotDir):
        os.mkdir(sampleScreenshotDir)
    for key, value in mergeDirData.items():
        for sample in mergeDirTests:
            name = sample["name"].split(".")[0]
            nameSplit = sample["name"].split("_")
            nameSplit[0] = str(index)
            newName = "_".join(nameSplit)
            copyfile(path + sample["name"], "./samples/" + newName)
            sample["name"] = newName
            newName = newName.split(".")[0]
            oldScreenDir = mergeScreenshotDir + name + "/"
            newScreenDir = sampleScreenshotDir + newName + "/"
            if find(sample["original_name"]):
                print("That sample already exists")
                exit(1)
            os.mkdir(newScreenDir)
            for screen in os.listdir(oldScreenDir):
                copyfile(oldScreenDir + screen, newScreenDir + screen)
            index += 1
            if key in sampleData:
                sampleData.append(sample)
            else:
                sampleData[key].append(sample)
            print("Added {0} -> {1}".format(name, newName))
    setVersion(sampleData, VersionIncrement.MINOR)
    sampleHandler.setJsonObject(sampleData)
    sampleHandler.closeJsonObject()


def find(filepath, mode):
    print(str.format("Looking for {0} - {1} (path: {2})", filepath, mode, getFileName(filepath)))
    jHandler = JsonHandler()
    jsonData = jHandler.getJsonObject()
    for currentMode, testList in jsonData.items():
        if (currentMode != "profile"):
            for element in testList:
                if element["original_name"] == getFileName(filepath) and mode == currentMode:
                    return True
    return False


def getFromConfig(filename):
    jHandle = JsonHandler()
    jsonData = jHandle.getJsonObject()
    for element in jsonData:
        if element["name"] == filename:
            return element
    return None


def listNames():
    jHandle = JsonHandler()
    jsonData = jHandle.getJsonObject()
    jHandle.closeJsonObject()
    print(jsonData)
    for key, value in jsonData.items():
        if key != "profile":
            print("{0}: ", key)
            for element in value:
                print("\t" + element["name"] +
                    " -> (" + element["original_name"] + ")")


def updateFileName(filename):
    if (filename.find("/") == -1):
        filename = os.getcwd() + "/" + filename
    elif (filename[0] == '.' and filename[1] == '/'):
        filename = os.getcwd() + "/" + filename[2:]
    return filename


addTypeValue = dict()
addTypeValue["manual"] = AddType.MANUAL
addTypeValue["auto"] = AddType.AUTO
addTypeValue["hard"] = AddType.HARD
addTypeValue["speed"] = AddType.SPEED
addTypeValue["playback"] = AddType.PLAYBACK

description = "Script to manage the VLCBenchmark sample list"
parser = argparse.ArgumentParser(description=description,
                                 formatter_class=argparse.RawTextHelpFormatter)

sampleEditionGroup = parser.add_argument_group("Sample edition options")
sampleEditionGroup.add_argument('--add', dest="addSample",
                                help="Add a new sample to the VLCBenchmark sample list")
sampleEditionGroup.add_argument('--edit', dest="editSample",
                                help="edit the screenshots of a sample from the VLCBenchmark sample list")
modeHelp = "add / edition modes: \n"
modeHelp += "\tmanual:\tmanually select screenshot timestamps using space while"
modeHelp += " vlc plays the sample, then screenshot using the thumbnailer.\n"
modeHelp += "\tauto:\tautomatically takes screenshots at percentages 25, 50, "
modeHelp += "and 75 of the sample duration\n"
modeHelp += "\thard:"
modeHelp += "\ttake screenshots pressing space while vlc plays the sample.\n"
modeHelp += "\t\tThis mode is intented for samples for which the thumbnailer "
modeHelp += "has trouble, because of seek issues.\n"
modeHelp += "\tspeed: "
modeHelp += "this test will run playback, and speed up or down depending on "
modeHelp += "whether there are drops in playback, and converge on the maximum "
modeHelp += "playback speed possible"
sampleEditionGroup.add_argument('--mode', dest='mode', nargs=1,
                                required='--add' in sys.argv or '--edit' in sys.argv,
                                choices=addTypeValue.keys(), help=modeHelp)
sampleEditionGroup.add_argument('--vlc', dest='vlcPath',
                                default="vlc",
                                help="path to an alternative vlc binary for hard mode")
sampleEditionGroup.add_argument('--delete', dest="delSample",
                                help="delete a sample from the VLCBenchmark sample list")
sampleEditionGroup.add_argument('--merge', dest="mergeDir",
                                help="merge a configuration with the sample directory")
sampleEditionGroup.add_argument('--keep-name', dest="keepOriginalName", default=False,
                                action="store_true",
                                help="keep original filename vs replace it by mediainfo")

miscGroup = parser.add_argument_group("Miscellaneous")
miscGroup.add_argument('--list', dest='boolList', default=False,
                       action="store_true",
                       help="list all samples.")
miscGroup.add_argument('--checksum', dest='boolChecksum', default=False,
                       action="store_true",
                       help="check the integrity off all files in the sample list")

args = parser.parse_args()
if (args.vlcPath != None):
    vlcPath = args.vlcPath
if (args.addSample and args.mode != None):
    add(args.addSample, addTypeValue[args.mode[0]], args.keepOriginalName)
elif (args.editSample and args.mode != None):
    edit(args.editSample, addTypeValue[args.mode[0]])
elif (args.mergeDir):
    mergeDir(args.mergeDir)
elif (args.delSample):
    delete(args.delSample)
elif (args.boolList):
    listNames()
elif (args.boolChecksum):
    checkChecksum()
else:
    parser.print_help()
