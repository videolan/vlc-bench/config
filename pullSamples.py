#!/usr/bin/env python3

import json
import subprocess
import os
import hashlib

def get_checksum(filepath):
    filestr = open(filepath, "rb")
    hexhash = hashlib.sha512(filestr.read()).hexdigest()
    return hexhash

def get_conf( url ):
    if not os.path.exists( "config.json" ):
        subprocess.call( [ "wget", url  + "config.json" ] )

def wget( url, conf ):
    if os.path.exists( conf[ "url" ] ):
        print( "File exists" )
        if get_checksum( conf[ "url" ] ) == conf[ "checksum" ]:
            return
        else:
            print( "Failed checksum test\nRedownloading ... " )
            os.remove( conf[ "url" ] )
    subprocess.call( [ "wget", url + conf[ "url" ] ] )


def load_json( filename ):
    try:
        jsonfile = open( filename, "r+" )
        jsonstr = jsonfile.read()
        data = json.loads( jsonstr )
    except Exception as e:
        print( "Failed to load file:  {0}".format( e ) )
        return None
    return data

def getSamplesFromTestCategory(filelist, conf, category):
    if category not in conf:
        return filelist
    for i in range( len( conf[category] ) ):
        if conf[category][i]["name"] not in filelist:
            filelist.append(conf[category][i])
    return filelist

def getSampleList(conf):
    filelist = getSamplesFromTestCategory(list(), conf, "playback")
    filelist = getSamplesFromTestCategory(filelist, conf, "quality")
    return getSamplesFromTestCategory(filelist, conf, "speed")


url = "https://streams.videolan.org/benchmark/"
get_conf( url )
conf = load_json( "config.json" )

filelist = getSampleList(conf)
for i in range( len( filelist ) ):
    print( "element {0} / {1}: ".format( i, len( filelist ) ) )
    wget( url, filelist[ i ] )
