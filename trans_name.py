#!/usr/bin/env python3

# Script that lists all files in the samples directory
# and changes recomputes their name based on the file's mediainfo

import subprocess
import os
import utils

sdir = os.getcwd() + "/samples/"
conf = utils.load_json( sdir + "config.json" )
for i in range( len( conf ) ):
    csvinfo = utils.get_mediainfo( sdir + conf[ i ][ "name" ] )
    name = utils.get_name_from_mediainfo( i + 1, csvinfo, conf[ i ][ "name" ].split(".")[-1] )
    print( "{0} --> {1}\n".format( conf[ i ][ "name" ], name ) )
    os.rename( sdir + conf[ i ][ "name" ], sdir + name )
    os.rename( sdir + "screenshots/" + conf[ i ][ "name" ].split( "." )[ 0 ],
            sdir + "screenshots/" + name.split( "." )[ 0 ] )
    conf[ i ][ "name" ] = name
    conf[ i ][ "url" ] = name
utils.save( sdir + "config.json", conf )
