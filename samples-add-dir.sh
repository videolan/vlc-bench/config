#!/bin/bash

# Loop on content of directory and add files to benchmark samples

if [ "$#" -eq 0 ] ; then
    echo "usage: $0 directory"
    exit 1
fi

echo "Looping on content of directory: $1"
for FILE in `ls $1` ; do
    FILEPATH="$1/$FILE"
    echo "Adding $FILEPATH ..."
    ./bench_config.py --add-auto $FILEPATH 
done
