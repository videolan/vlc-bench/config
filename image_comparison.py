#!/usr/bin/env python3

from PIL import Image

#number of blocks in width
wb_number = 6
#number of blocks in height
hb_number = 5

def getIntFromRGB(rgb):
    red = int( rgb[0] )
    green = int( rgb[1] )
    blue = int( rgb[2] )
    RGBint = int(red << 16) | int(green << 8) | int(blue)
    return RGBint

def getBlockColorValue(pix, imageSize, iWidth, iHeight):
    wb_size = int( imageSize[0] / wb_number )
    hb_size = int( imageSize[1] / hb_number )

    red = 0
    green = 0
    blue = 0

    for i in range (iWidth * wb_size, (iWidth + 1) * wb_size):
        for inc in range (iHeight * hb_size, (iHeight + 1) * hb_size):
            red += pix[i, inc][0]
            green += pix[i, inc][1]
            blue += pix[i, inc][2]
    red /= wb_size * hb_size
    green /= wb_size * hb_size
    blue /= wb_size * hb_size
    return (getIntFromRGB([red, green, blue]))

def getColorValues(filename):
        # image = cropUselessSides(filename)
        image = Image.open(filename)
        newImage = image.convert('YCbCr')
        pix = newImage.load()
        # pix = image.load()
        # print(pix.__dict__)
        imageSize = image.size

        colorValues = []

        for i in range(0, wb_number):
                for inc in range(0, hb_number):
                        colorValues.append(getBlockColorValue(pix, imageSize, i, inc))

        return colorValues