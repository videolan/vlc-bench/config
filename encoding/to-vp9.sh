#!/bin/bash

#vp9 generation script

#requires libvpx
# https://github.com/webmproject/libvpx/

if [ "$#" -ne 1 -a "$#" -ne 2 ] ; then
    echo "usage: $0 [filename] [secondFilename optional]"
    exit 1
fi

FILE_1=$1
FILENAME_1=(${FILE_1//./ })

if [ "$#" -eq 1 ] ; then
    FILE_2=$FILE_1
    FILENAME_2=$FILENAME_1
else
    FILE_2=$2
    FILENAME_2=(${FILE_2//./ })
fi

# resolution array for output file name
# RES[0]="-w 1280 -h 720"
# RES[1]="-w 1920 -h 1080"
RES[0]="-w 3840 -h 2160"

# pretty resolution array for output file name
# PRES[0]="1280x720"
# PRES[1]="1920x1080"
PRES[0]="3840x2160"

# fps to test array
FPS[0]="25/1"
FPS[1]="30/1"
FPS[2]="60/1"

# pretty fps for file name
PFPS[0]="25"
PFPS[1]="30"
PFPS[2]="60"

# bit depth
DEPTH[0]="--bit-depth=8 --profile=0"
DEPTH[1]="--bit-depth=10 --profile=2"
DEPTH[2]="--bit-depth=12 --profile=2"

# pretty bit depth for file name
PDEPTH[0]="8bits"
PDEPTH[1]="10bits"
PDEPTH[2]="12bits"

LEN_RES=${#RES[@]} # RES array length
LEN_FPS=${#FPS[@]} # FPS array length
LEN_DEPTH=${#DEPTH[@]} # DEPTH array length

I_FILE=0

I_DEPTH=0
while [ "$I_DEPTH" -lt $LEN_DEPTH ]
do
    I_RES=0
    echo "loop number $I_DEPTH"
    while [ "$I_RES" -lt $LEN_RES ]
    do
		I_FPS=0 # FPS Array index
		while [ "$I_FPS" -lt $LEN_FPS ]
		do
			if [ $I_RES -lt $LEN_RES -a $I_FPS -ne 2 -o $I_RES -eq 0 -a $I_FPS -eq 2 ] ; then

				# file swap
				if [ $I_FILE -eq 0 ] ; then
					FILENAME=$FILENAME_1
					FILE=$FILE_1
				else
					FILENAME=$FILENAME_2
					FILE=$FILE_2
				fi

				echo "${PRES[$I_RES]} - ${PFPS[$I_FPS]}"
				OUTPUT="${FILENAME[0]}_vp9_${PDEPTH[$I_DEPTH]}_${PRES[$I_RES]}_${PFPS[$I_FPS]}fps.webm"
				echo $OUTPUT
				PARAMS="--codec=vp9 --best \
					${DEPTH[$I_DEPTH]} \
					${RES[$I_RES]} \
					--fps=${FPS[$I_FPS]} \
					-o $OUTPUT $FILE"
				vpxenc $PARAMS
				I_FILE=`expr $I_FILE + 1`
				I_FILE=`expr $I_FILE % 2`
			fi
			I_FPS=`expr $I_FPS + 1`
		done
		I_RES=`expr $I_RES + 1`
    done
    I_DEPTH=`expr $I_DEPTH + 1`
done


#cli examples:
#vpxenc --codec=vp9 --bit-depth=8 --profile=0 -w 352 -h 240 --fps=300/10 --good -o in_to_tree_vp8_test.mkv in_to_tree_2160p50.y4m ->for 8 bits
#vpxenc --codec=vp9 --bit-depth=10 --profile=2 -w 352 -h 240 --fps=300/10 --good -o in_to_tree_vp8_test.mkv in_to_tree_2160p50.y4m ->for 10 bits
#vpxenc --codec=vp9 --bit-depth=12 --profile=2 -w 352 -h 240 --fps=300/10 --good -o in_to_tree_vp8_test.mkv in_to_tree_2160p50.y4m ->for 12 bits

# https://www.webmproject.org/vp9/profiles/
