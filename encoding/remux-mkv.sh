#!/bin/bash

#script to remux mkv files
#testing for this for `mkv demux error: unable to read KaxCluster during seek, giving up`

if [ "$#" -ne 1 ] ; then
    echo "usage: ./remux-mkv.sh [dirpath]"
    exit 1
fi

DIRPATH=$1
TMPPATH="$DIRPATH/tmpfile.mkv"
for FILE in `ls $DIRPATH` ; do
    FILEPATH="$DIRPATH/$FILE"
    echo "filepath: $FILEPATH"
    mv $FILEPATH $TMPPATH
    mkvmerge -o $FILEPATH $TMPPATH
done
