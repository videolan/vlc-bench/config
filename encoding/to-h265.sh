#!/bin/bash

#h265 sample generation script

if [ "$#" -ne 1 -a "$#" -ne 2 ] ; then
    echo "usage: $0 [filename]"
    exit 1
fi

FILE_1=$1
FILENAME_1=(${FILE_1//./ })

if [ "$#" -eq 1 ] ; then
    echo "Using one input file"
    FILE_2=$FILE
    FILENAME_2=$FILENAME_1
else
    echo "Using two input files"
    FILE_2=$2
    FILENAME_2=(${FILE_2//./ })
fi

# filename for x265 output that has to go through mkvmerge
TMP_OUTPUT="tmp.hevc"

# filename for ffmpeg downscale of raw input
TMP_Y4M="tmp.y4m"

# pretty resolution array for output file name
RES[0]="1280:720"
RES[1]="1920:1080"
RES[2]="3840:2160"

# pretty resolution array for output file name
PRES[0]="1280x720"
PRES[1]="1920x1080"
PRES[2]="3840x2160"

# fps to test array
FPS[0]=25
FPS[1]=30
FPS[2]=60

# The following levels are based on:
# https://en.wikipedia.org/wiki/High_Efficiency_Video_Coding#Tiers_and_levels

# 1280 x 720 @ 25 -> 3.1
# 1280 x 720 @ 30 -> 3.1
# 1280 x 720 @ 60 -> 4

# 1920 x 1080 @ 25 -> 4
# 1920 x 1080 @ 30 -> 4
# 1920 x 1080 @ 60 -> 4.1

# 3840 x 2160 @ 25 -> 5
# 3840 x 2160 @ 30 -> 5
# 3840 x 2160 @ 60 -> 5.1

declare -A LEVEL
LEVEL[0, 0]=3.1
LEVEL[0, 1]=3.1
LEVEL[0, 2]=4
LEVEL[1, 0]=4
LEVEL[1, 1]=4
LEVEL[1, 2]=4.1
LEVEL[2, 0]=5
LEVEL[2, 1]=5
LEVEL[2, 2]=5.1

# bit depth of output
DEPTH[0]="8"
DEPTH[1]="10"
DEPTH[2]="12"

# Profiles are associated to depth
PROFILE[0]="main"
PROFILE[1]="main10"
PROFILE[2]="main12"

LEN_RES=${#RES[@]} # RES array length
LEN_FPS=${#FPS[@]} # FPS array length
LEN_DEPTH=${#DEPTH[@]} # DEPTH array length

I_FILE=0

I_DEPTH=0
while [ "$I_DEPTH" -lt $LEN_DEPTH ]
do
    I_RES=0
    echo "loop number $I_DEPTH"
    while [ "$I_RES" -lt $LEN_RES ]
    do
	I_FPS=0 # FPS Array index
	while [ "$I_FPS" -lt $LEN_FPS ]
	do
	    if [ $I_RES -lt $LEN_RES -a $I_FPS -ne 2 -o $I_RES -eq 2 -a $I_FPS -eq 2 ]
		then
			if [ $I_FILE -eq 0 ] ; then
				FILENAME=$FILENAME_1
				FILE=$FILE_1
			else
				FILENAME=$FILENAME_2
				FILE=$FILE_2
			fi

			echo "DOWNSCALING: ffmpeg -i $FILE -vf scale=${RES[$I_RES]} $TMP_Y4M"
			#ffmpeg -i $FILE -s ${RES[$I_RES]} $TMP_Y4M

			TMP_PARAMS="--preset veryslow "
			TMP_PARAMS="$TMP_PARAMS --profile=${PROFILE[$I_DEPTH]}"
			TMP_PARAMS="$TMP_PARAMS --level-idc=${LEVEL[$I_RES, $I_FPS]}"
			TMP_PARAMS="$TMP_PARAMS --output-depth=${DEPTH[$I_DEPTH]}"
			TMP_PARAMS="$TMP_PARAMS --fps ${FPS[$I_FPS]}"
			TMP_PARAMS="$TMP_PARAMS --output $TMP_OUTPUT $TMP_Y4M"
			echo "HEVC ENCODING: x265 $TMP_PARAMS"
			#x265 $TMP_PARAMS
			OUTPUT="${FILENAME[0]}_h265_${DEPTH[$I_DEPTH]}bits"
			OUTPUT="${OUTPUT}_${PRES[$I_RES]}_${FPS[$I_FPS]}fps.mkv"

			PARAMS="-o $OUTPUT $TMP_OUTPUT"
			echo "MKVMERGING: mkvmerge -o $OUTPUT $TMP_OUTPUT"
			#mkvmerge $PARAMS
			I_FILE=`expr $I_FILE + 1`
			I_FILE=`expr $I_FILE % 2`
			rm $TMP_Y4M
			rm $TMP_OUTPUT
			echo
	    fi
	    I_FPS=`expr $I_FPS + 1`
	done
	I_RES=`expr $I_RES + 1`
    done
    I_DEPTH=`expr $I_DEPTH + 1`
done

# x265 --preset veryslow --profile main --fps 60 --input-res 1280x720 --output in_to_tree_test.h265 in_to_tree_2160p50.y4m
