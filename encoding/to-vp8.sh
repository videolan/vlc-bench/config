#!/bin/bash

#vp8 generation script

#requires libvpx
# https://github.com/webmproject/libvpx/

if [ "$#" -ne 1 -a "$#" -ne 2 ] ; then
    echo "usage: $0 [filename] [secondFilename optional]"
    exit 1
fi

FILE_1=$1
FILENAME_1=(${FILE_1//./ })

if [ "$#" -eq 1 ] ; then
    FILE_2=$FILE_1
    FILENAME_2=$FILENAME_1
else
    FILE_2=$2
    FILENAME_2=(${FILE_2//./ })
fi

# resolutions to test array
# RES[0]="-w 352 -h 240"
# RES[1]="-w 480 -h 360"
# RES[2]="-w 858 -h 480"
# RES[3]="-w 1280 -h 720"
# RES[4]="-w 1920 -h 1080"
RES[0]="-w 3840 -h 2160"

# pretty resolution array for output file name
# PRES[0]="352x240"
# PRES[1]="480x360"
# PRES[2]="858x480"
# PRES[3]="1280x720"
# PRES[4]="1920x1080"
PRES[0]="3840x2160"

# fps to test array
FPS[0]="25/1"
FPS[1]="30/1"
FPS[2]="60/1"

# pretty fps for file name
RFPS[0]="25"
RFPS[1]="30"
RFPS[2]="60"

# There will be two loops below, one for vp8_8bits
# the other for vp8_10bits, in which we only
# want to test resolutions higher than 480p,
# hence the 3 as a starting index while looping on RES array
RES_START[0]=0
RES_START[1]=0

LEN_RES=${#RES[@]} # RES array length
LEN_FPS=${#FPS[@]} # FPS array length
LEN_RES_START=${#RES_START[@]} # RES_START array length

I_FILE=0

echo "test"
I_RES_START=0
while [ "$I_RES_START" -lt $LEN_RES_START ]
do
    I_RES=${RES_START[$I_RES_START]}
    echo "loop number $I_RES_START"
    while [ "$I_RES" -lt $LEN_RES ]
    do
		I_FPS=0 # FPS Array index
		while [ "$I_FPS" -lt $LEN_FPS ]
		do
			if [ $I_RES -lt $LEN_RES -a $I_FPS -ne 2 -o $I_RES -eq 0 -a $I_FPS -eq 2 ] ; then
				if [ $I_FILE -eq 0 ] ; then
					FILENAME=$FILENAME_1
					FILE=$FILE_1
				else
					FILENAME=$FILENAME_2
					FILE=$FILE_2
				fi

				echo "${PRES[$I_RES]} - ${RFPS[$I_FPS]}"
				OUTPUT="${FILENAME[0]}_vp8_${PRES[$I_RES]}_${RFPS[$I_FPS]}fps.webm"
				echo $OUTPUT
				PARAMS="--codec=vp8 ${RES[$I_RES]} --fps=${FPS[$I_FPS]} --best \
					-o $OUTPUT $FILE"
				vpxenc $PARAMS
				I_FILE=`expr $I_FILE + 1`
				I_FILE=`expr $I_FILE % 2`
			fi
			I_FPS=`expr $I_FPS + 1`
		done
		I_RES=`expr $I_RES + 1`
    done
    I_RES_START=`expr $I_RES_START + 1`
done

# cli example
# vpxenc --codec=vp8 -w 1280 -h 720 --fps=300/10 --best -o in_to_tree_vp8_test.mkv in_to_tree_2160p50.y4m
