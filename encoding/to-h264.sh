#!/bin/bash

#h264 sample generation script

if [ "$#" -ne 1 -a "$#" -ne 2 ] ; then
    echo "usage: $0 [filename]"
    exit 1
fi

FILE_1=$1
FILENAME_1=(${FILE_1//./ })

if [ "$#" -eq 1 ] ; then
    echo "Using one input file"
    FILE_2=$FILE
    FILENAME_2=$FILENAME_1
else
    echo "Using two input files"
    FILE_2=$2
    FILENAME_2=(${FILE_2//./ })
fi

# resolutions to test array
RES[0]="width=352,height=240"
RES[1]="width=480,height=360"
RES[2]="width=858,height=480"
RES[3]="width=1280,height=720"
RES[4]="width=1920,height=1080"
RES[5]="width=3840,height=2160"

# pretty resolution array for output file name
PRES[0]="352x240"
PRES[1]="480x360"
PRES[2]="858x480"
PRES[3]="1280x720"
PRES[4]="1920x1080"
PRES[5]="3840x2160"

# fps to test array
FPS[0]=25
FPS[1]=30
FPS[2]=60

# The following levels are based on:
# https://en.wikipedia.org/wiki/Advanced_Video_Coding#Profiles

# This resolution isn't described
# instead it will be based on the 352 x 576 res which is fairly equivalent
# in total surface: 352x288=101,376 & 352x240=84,480
# 352 x 240 @ 25 -> 1.3
# 352 x 240 @ 30 -> 1.3
# 352 x 240 @ 60 -> 2

# This resolution isn't described
# instead it will be based on the 352 x 576 res which is fairly equivalent
# in total surface: 352x576=202,752 & 480x360=172,800
# 480 x 360 @ 25 -> 2.2
# 480 x 360 @ 30 -> 3
# 480 x 360 @ 60 -> 3.1

# This resolution isn't described
# instead it will be based on the 720 x 576 res which is fairly equivalent
# in total surface: 720x576=414,720 & 858x480=411,840
# 858 x 480 @ 25 -> 3
# 858 x 480 @ 30 -> 3.1
# 858 x 480 @ 60 -> 3.1

# 1280 x 720 @ 25 -> 3.1
# 1280 x 720 @ 30 -> 3.1
# 1280 x 720 @ 60 -> 3.2

# 1920 x 1080 @ 25 -> 4.1
# 1920 x 1080 @ 30 -> 4.1
# 1920 x 1080 @ 60 -> 4.2

# 3840 x 2160 @ 25 -> 5.1
# 3840 x 2160 @ 30 -> 5.1
# 3840 x 2160 @ 60 -> 5.2

# This represents profile levels: LEVEL[I_RES, I_FPS]
declare -A LEVEL
LEVEL[0, 0]=1.3
LEVEL[0, 1]=1.3
LEVEL[0, 2]=2
LEVEL[1, 0]=2.2
LEVEL[1, 1]=3
LEVEL[1, 2]=3.1
LEVEL[2, 0]=3
LEVEL[2, 1]=3.1
LEVEL[2, 2]=3.1
LEVEL[3, 0]=3.1
LEVEL[3, 1]=3.1
LEVEL[3, 2]=3.2
LEVEL[4, 0]=4.1
LEVEL[4, 1]=4.1
LEVEL[4, 2]=4.2
LEVEL[5, 0]=5.1
LEVEL[5, 1]=5.1
LEVEL[5, 2]=5.2

# There will be two loops below, one for H264_8bits
# the other for H264_10bits, in which we only
# want to test resolutions higher than 480p,
# hence the 3 as a starting index while looping on RES array
# Each bit depth comes with a specific profile
RES_START[0]=0
RES_START[1]=3

DEPTH[0]=8
DEPTH[1]=10

PROFILE[0]="main"
PROFILE[1]="high10"

LEN_RES=${#RES[@]} # RES array length
LEN_FPS=${#FPS[@]} # FPS array length
LEN_RES_START=${#RES_START[@]} # RES_START array length

I_FILE=0

I_RES_START=0
while [ "$I_RES_START" -lt $LEN_RES_START ]
do
    I_RES=${RES_START[$I_RES_START]}
    echo "loop number $I_RES_START"
    while [ "$I_RES" -lt $LEN_RES ]
    do
		I_FPS=0 # FPS Array index
		while [ "$I_FPS" -lt $LEN_FPS ]
		do
			if [ $I_RES -lt $LEN_RES -a $I_FPS -ne 2 -o $I_RES -eq 5 -a $I_FPS -eq 2 ] ; then
			if [ $I_FILE -eq 0 ] ; then
				FILENAME=$FILENAME_1
				FILE=$FILE_1
			else
				FILENAME=$FILENAME_2
				FILE=$FILE_2
			fi

			OUTPUT="${FILENAME[0]}_h264_${DEPTH[$I_RES_START]}bits"
			OUTPUT="${OUTPUT}_${PRES[$I_RES]}_${FPS[$I_FPS]}fps.mkv"

			PARAMS="--output $OUTPUT"
			PARAMS="$PARAMS --profile ${PROFILE[$I_RES_START]}"
			PARAMS="$PARAMS --output-depth ${DEPTH[$I_RES_START]}"
			PARAMS="$PARAMS --level ${LEVEL[$I_RES, $I_FPS]}"
			PARAMS="$PARAMS --preset veryslow --fps ${FPS[$I_FPS]}"
			PARAMS="$PARAMS --video-filter resize:${RES[$I_RES]}"
			PARAMS="$PARAMS $FILE"
			echo "x264 $PARAMS"
			x264 $PARAMS
			echo "Output: $OUTPUT"
			echo
			I_FILE=`expr $I_FILE + 1`
			I_FILE=`expr $I_FILE % 2`
			fi
			I_FPS=`expr $I_FPS + 1`
		done
		I_RES=`expr $I_RES + 1`
    done
    I_RES_START=`expr $I_RES_START + 1`
done
