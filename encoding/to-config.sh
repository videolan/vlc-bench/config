#!/bin/bash

#script to use the bench_config.py script and add all media
#files in a directory to the benchmark json config file

if [ "$#" -ne 1 ] ; then
    echo "usage: ./to-config [dirpath]"
    exit 1
fi

DIRPATH=$1
for FILE in `ls $DIRPATH` ; do
    FILEPATH="$DIRPATH/$FILE"
    echo "filepath: $FILEPATH"
    ./bench_config.py --add-auto $FILEPATH
done
