#!/bin/bash

if [ "$#" -ne 1 -a "$#" -ne 2 ] ; then
    echo "usage: $0 [filename]"
    exit 1
fi

FILE_1=$1
FILENAME_1=(${FILE_1//./ })

if [ "$#" -eq 1 ] ; then
    echo "Using one input file"
    FILE_2=$FILE
    FILENAME_2=$FILENAME_1
else
    echo "Using two input files"
    FILE_2=$2
    FILENAME_2=(${FILE_2//./ })
fi

RES[0]="352x240"
RES[1]="480x360"
RES[2]="858x480"
RES[3]="1280x720"
RES[4]="1920x1080"

FPS[0]=25
FPS[1]=30

LEN_RES=${#RES[@]}
LEN_FPS=${#FPS[@]}

I_FILE=0

I_RES=0
while [ "$I_RES" -lt $LEN_RES ]
do
    I_FPS=0
    while [ "$I_FPS" -lt $LEN_FPS ]
    do
	if [ $I_FILE -eq 0 ] ; then
	    FILENAME=$FILENAME_1
	    FILE=$FILE_1
	else
	    FILENAME=$FILENAME_2
	    FILE=$FILE_2
	fi
	if [ $I_RES -lt 3 ] ; then
    	    OUTPUT="${FILENAME[0]}_mpeg4_8bits_${RES[$I_RES]}_${FPS[$I_FPS]}fps_r3M.mkv"
	    PARAMS="-i $FILE -s ${RES[$I_RES]} -r ${FPS[I_FPS]} -c:v libxvid -vb 3M $OUTPUT"
	else
    	    OUTPUT="${FILENAME[0]}_mpeg4_8bits_${RES[$I_RES]}_${FPS[$I_FPS]}fps_r10M.mkv"
	    PARAMS="-i $FILE -s ${RES[$I_RES]} -r ${FPS[I_FPS]} -c:v libxvid -vb 10M $OUTPUT"
	fi
	echo "ffmpeg $PARAMS"
	ffmpeg $PARAMS
	I_FILE=`expr $I_FILE + 1`
	I_FILE=`expr $I_FILE % 2`
	I_FPS=`expr $I_FPS + 1`
    done
    I_RES=`expr $I_RES + 1`
done
