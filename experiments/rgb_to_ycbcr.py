#!/usr/bin/env python3

# script that converts rgb block signatures to YCbCr block signatures

import os
import utils
import image_comparison as ic

sdir = os.getcwd() + "/samples/"
print("Using sample directory: {0}".format(sdir))
conf = utils.load_json( sdir + "config.json" )
for i in range( len( conf ) ):
    name = conf[i]["name"].split(".")[0]
    print("Updating scores for sample: {0}".format(name))
    screenDir = sdir + "screenshots/" + name + "/"
    print("Using screen directory: {0}".format(screenDir))
    if os.path.exists( screenDir ):
        if os.path.isdir( screenDir ):
            for inc in range( len( conf[i]["snapshot"] ) ):
                screenshot = screenDir + "screenshot_" + str(inc) + ".png"
                conf[i]["snapshot"][inc][1] = ic.getColorValues(screenshot)
        else:
            print("Error: screen dir is not a directory")
            exit(1)
    else:
        print("Error: screen dir doesn't exist")
        exit(1)
utils.save(sdir + "config.json", conf)
    