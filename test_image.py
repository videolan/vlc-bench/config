#!/usr/bin/env python3

# Script that calculates the color difference between two images
# using the benchmark comparison algorithm

import argparse

import image_comparison as ic

def getDiffPercentage(colorValue, reference):
    return abs(reference - colorValue) / 255.0 * 100.0

def getRGBFromInt( val ):
    colors = dict()
    colors["red"] = ( val & 0xFF0000 ) >> 16
    colors["green"] = ( val & 0x00FF00 ) >> 8
    colors["blue"] = val & 0x0000FF
    return colors

def compareBlockValues(block, reference):
    blockColor = getRGBFromInt(block)
    refColor = getRGBFromInt(reference)
    red = getDiffPercentage(blockColor["red"], refColor["red"])
    green = getDiffPercentage(blockColor["green"], refColor["green"])
    blue = getDiffPercentage(blockColor["blue"], refColor["blue"])
    return (red + green + blue) / 3.0

def compareAllBlocks(blocks, reference):
    if len(blocks) != len(reference):
        return
    res = 0
    for i in range(len(blocks)):
        diff = compareBlockValues(blocks[i], reference[i])
        res += diff
    print("Res: {0}".format(res))

description = "Computes the difference between two images"
parser = argparse.ArgumentParser(description=description)
parser.add_argument('reference', help="reference image path")
parser.add_argument('copy', help="copy image path")
args = parser.parse_args()
ref = args.reference
copy = args.copy

ref_sig = ic.getColorValues(ref)
copy_sig = ic.getColorValues(copy)

compareAllBlocks(copy_sig, ref_sig)
