#!/usr/bin/env python3

# Sums the snaphot count in samples
# ex output: {3: 121, 4: 3, 2: 3}

import os
import json

def load_json( filename ):
    try:
        jsonfile = open( filename, "r+" )
        jsonstr = jsonfile.read()
        data = json.loads( jsonstr )
        jsonfile.close()
    except Exception as e:
        print( "Failed to load file:  {0}".format( e ) )
        return None
    return data

sdir = "./samples/"
conf = load_json( sdir + "config.json" )
nd = dict()
for i in range( len( conf ) ):
    size = len( conf[ i ][ "snapshot" ] )
    if size not in nd.keys():
        nd[ size ] = 0
    nd[ size ] += 1
print( nd )
