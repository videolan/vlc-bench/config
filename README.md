vlcbench-backend
================
VLCBenchmark is a benchmark application focused on testing android devices' video capabilities using the VLC Media Player.
It runs a test suit of video samples encoded according to many different parameters to see what works and what doesn't.
It then rates the device according these tests, and allows you to upload the results to this backend, for everyone to see and compare devices.

## Benchmark video samples handling

The VLCBenchmark requires samples and a json config file, detailling
the sample list, to work. These files are hosted at:
http://streams.videolan.org/benchmark/

There is a python 3.7 script bench_config.py.
This script interacts with the config file to add or delete samples,
list, etc ...

This script will create a new sample directory at it's position if
there isn't any, so to add or delete samples from the active
benchmark configuration, you have to download the content of the
benchmark directory on streams.videolan.org

You can also add all a directory at once using the to-config.sh script.
It loops on the directory's content using the bench_config.py's auto-mode

To upload the modified config file, and the new samples:
http://streams.videolan.org/upload/

Fill the form as below:
```
Project: Videolan
VLC Version: latest
Description: benchmark
```

Then ask a VideoLAN ftp masters (Etix or Thresh)to move the files.

## Encoding samples

For this project we needed to encode some samples.
Some scripts, in the form of `to-X.sh`, are provided in `misc`.
They encode samples with the codec based on the name of the script,
changing `resolution`, `fps`, `color depth`.
As parameters, they can take one sample as raw source or two and alternate.

## Mediainfo overview on samples

If you want to get an overview on sample media information,
there is a script that will generate a csv file, listing
some media information of all files in a directory: `misc/getMediaCsv.py`
```
Usage: ./getMediaCSV.py [file|dirpath] [csvfile]
```
