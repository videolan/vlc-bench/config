#!/usr/bin/env python3

import argparse
from PIL import Image

def isPixelTransparent(pixel):
    color = pixel[0] << 24 or pixel[1] << 16 or pixel[2] << 8 or pixel[3]
    return not bool(color)

def cropUselessSides(filename):
    image = Image.open(filename)
    pixels = image.load()
    # pos = (400, 0)
    # print("Pix[0][red]: {0}".format(hex(pixels[pos][0])))
    # print("Pix[0][green]: {0}".format(hex(pixels[pos][1])))
    # print("Pix[0][blue]: {0}".format(hex(pixels[pos][2])))
    # print("Pix[0][other]: {0}".format(hex(pixels[pos][3])))
    i = 0
    while isPixelTransparent(pixels[(i, 0)]):
        i += 1
    minWidth = i
    print("minWidth: {0}".format(minWidth))
    i = image.size[0] - 1
    while isPixelTransparent(pixels[(i, 0)]):
        i -= 1
    maxWidth = i
    print("maxWidth: {0}".format(maxWidth))
    if minWidth == 0 and maxWidth + 1 == image.size[0]:
        print("No need to crop")
        return image
    cropped_image = image.crop((minWidth, 0, maxWidth, image.size[1]))
    cropped_image.save("output.png")
    return cropped_image

description = "Crops transparent lines on the sides of an image"
parser = argparse.ArgumentParser(description=description)
parser.add_argument('filename', help="file to crop")
args = parser.parse_args()
filename = args.filename
cropUselessSides(filename)