#!/usr/bin/env python3

import argparse
import json
import shutil
import os


def load(filename):
    try:
        jsonfile = open(filename, "r+")
        jsonstr = jsonfile.read()
        data = json.loads(jsonstr)
        jsonfile.close()
    except Exception as e:
        print("Failed to load file:  {0}".format(e))
        return None
    return data


def save(filename, index):
    jsonfile = open(filename, "w+")
    datastr = json.dumps(index, indent=4, separators=(",", ": "))
    jsonfile.seek(0)
    jsonfile.write(datastr)
    jsonfile.truncate()
    jsonfile.close()


description = "Script to migrate config folder from 0.15 to 0.16 data format"
parser = argparse.ArgumentParser(description=description)
parser.add_argument('filepath', help='Absolute path to config file to migrate')
args = parser.parse_args()
path = args.filepath

if (os.path.exists("./samples")):
    print("There already is a generated samples directory")
    exit(1)
jsondata = load(path + "/config.json")
quality = list()
playback = list()
for v in jsondata:
    print(v)
    software_quality = dict()
    software_playback = dict()
    hardware_quality = dict()
    hardware_playback = dict()

    software_quality["name"] = v["name"]
    software_playback["name"] = v["name"]
    hardware_quality["name"] = v["name"]
    hardware_playback["name"] = v["name"]

    software_quality["hardware"] = False
    software_playback["hardware"] = False
    hardware_quality["hardware"] = True
    hardware_playback["hardware"] = True

    software_quality["size"] = v["size"]
    software_playback["size"] = v["size"]
    hardware_quality["size"] = v["size"]
    hardware_playback["size"] = v["size"]

    software_quality["url"] = v["url"]
    software_playback["url"] = v["url"]
    hardware_quality["url"] = v["url"]
    hardware_playback["url"] = v["url"]

    software_quality["original_name"] = v["original_name"]
    software_playback["original_name"] = v["original_name"]
    hardware_quality["original_name"] = v["original_name"]
    hardware_playback["original_name"] = v["original_name"]

    software_quality["checksum"] = v["checksum"]
    software_playback["checksum"] = v["checksum"]
    hardware_quality["checksum"] = v["checksum"]
    hardware_playback["checksum"] = v["checksum"]

    software_quality["snapshot"] = v["snapshot"]
    hardware_quality["snapshot"] = v["snapshot"]

    quality.append(software_quality)
    quality.append(hardware_quality)

    playback.append(software_playback)
    playback.append(hardware_playback)

newJsonData = dict()
newJsonData["quality"] = quality
newJsonData["playback"] = playback

shutil.copytree(path + "", "./samples")
os.remove("./samples/config.json")
save("./samples/config.json", newJsonData)
